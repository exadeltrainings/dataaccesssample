﻿using DataAccessSample.Data.AdoNet;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.ConsoleApp.Samples
{
    // TODO: create the code to run samples with full CRUD operation set with test data set!

    /// <summary>
    /// Samples to work with data in database with ADO.NET.
    /// </summary>
    public class AdoNetSample : SampleBase
    {
        private readonly IDataFactory _dataFactory;
        private readonly IProductsRepository _productsRepository;
        private readonly ICustomersRepository _customersRepository;
        private readonly IOrdersRepository _ordersRepository;

        public AdoNetSample()
        {
            _dataFactory = new AdoNetDataFactory(AppData.Configuration.DatabaseConfiguration.ConnectionString);

            // create concrete repositories to has access to tables with data
            _productsRepository = _dataFactory.CreateProductsRepository();
            _ordersRepository = _dataFactory.CreateOrdersRepository();
            _customersRepository = _dataFactory.CreateCustomersRepository();
        }

        public override void InsertData()
        {
        }

        public override void LoadData()
        {
            var products = _productsRepository.GetAll();

            PrintProducts(products);
        }
        
        public override void UpdateData()
        {
        }

        public override void DeleteData()
        {
        }

        public override void Run()
        {
            InsertData();

            LoadData();

            UpdateData();

            DeleteData();
        }
    }
}
