﻿using DataAccessSample.Data.Common.Interfaces;
using DataAccessSample.Data.EntLib;

namespace DataAccessSample.ConsoleApp.Samples
{
    public class EntLibSamples : SampleBase
    {
        private readonly IDataFactory _dataFactory;
        private readonly IProductsRepository _productsRepository;
        private readonly ICustomersRepository _customersRepository;
        private readonly IOrdersRepository _ordersRepository;

        public EntLibSamples()
        {
            _dataFactory = new EntLibDataFactory(AppData.Configuration.DatabaseConfiguration.ConnectionString);

            // create concrete repositories to has access to tables with data
            _productsRepository = _dataFactory.CreateProductsRepository();
            _ordersRepository = _dataFactory.CreateOrdersRepository();
            _customersRepository = _dataFactory.CreateCustomersRepository();
        }

        public override void InsertData()
        {
        }

        public override void LoadData()
        {
            var products = _productsRepository.GetAll();

            PrintProducts(products);
        }

        public override void UpdateData()
        {
        }

        public override void DeleteData()
        {
        }

        public override void Run()
        {
            InsertData();

            LoadData();

            UpdateData();

            DeleteData();
        }
    }
}
