﻿using DataAccessSample.Data.Common.Interfaces;
using DataAccessSample.Data.EntityFramework;

namespace DataAccessSample.ConsoleApp.Samples
{
    public class EntityFrameworkSamples : SampleBase
    {
        private readonly IDataFactory _dataFactory;
        private readonly IProductsRepository _productsRepository;
        private readonly ICustomersRepository _customersRepository;
        private readonly IOrdersRepository _ordersRepository;

        public EntityFrameworkSamples()
        {
            _dataFactory = new EntityFrameworkDataFactory(AppData.Configuration.DatabaseConfiguration.ConnectionString);

            // create concrete repositories to has access to tables with data
            _productsRepository = _dataFactory.CreateProductsRepository();
            _ordersRepository = _dataFactory.CreateOrdersRepository();
            _customersRepository = _dataFactory.CreateCustomersRepository();
        }

        public override void InsertData()
        {
            throw new System.NotImplementedException();
        }

        public override void LoadData()
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateData()
        {
            throw new System.NotImplementedException();
        }

        public override void DeleteData()
        {
            throw new System.NotImplementedException();
        }

        public override void Run()
        {
            InsertData();

            LoadData();

            UpdateData();

            DeleteData();
        }
    }
}
