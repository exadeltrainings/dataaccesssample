﻿using System;
using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;

namespace DataAccessSample.ConsoleApp.Samples
{
    public abstract class SampleBase
    {
        public abstract void Run();

        public abstract void InsertData();

        public abstract void LoadData();

        public abstract void UpdateData();

        public abstract void DeleteData();

        protected void PrintProducts(IEnumerable<Product> products)
        {
            int i = 1;
            foreach (var product in products)
            {
                Console.WriteLine("Product #{0}: Name - {1}, Cost - {2}.", i, product.Name, product.Cost);

                i++;
            }
        }
    }
}
