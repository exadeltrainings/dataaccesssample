﻿using System;
using DataAccessSample.ConsoleApp.Helpers;
using DataAccessSample.ConsoleApp.Samples;

namespace DataAccessSample.ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                InitializeApplication();

                DoSamples();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private static void InitializeApplication()
        {
            // load configuration from settings file
            var configuration = AppHelper.LoadConfiguration();

            // save config to static app data
            AppData.Configuration = configuration;
        }

        private static void DoSamples()
        {
            // do some samples
            // 1. Samples with ADO.NET.
            RunAdoNetSample();

            // 2. Samples with Enterprise Library.
            RunEntLibSamples();

            // 3. Samples with Entity Framework.

        }

        private static void RunAdoNetSample()
        {
            Console.WriteLine("Running ADO.NET samples...");

            var sample = new AdoNetSample();
            sample.Run();

            Console.WriteLine("Ends of ADO.NET samples.");
        }

        private static void RunEntLibSamples()
        {
            Console.WriteLine("Running Enterprise Library samples...");

            var sample = new EntLibSamples();
            sample.Run();

            Console.WriteLine("Ends of Enterprise Library samples.");
        }
    }
}
