﻿using System.Configuration;
using DataAccessSample.Common.Configuration;

namespace DataAccessSample.ConsoleApp.Helpers
{
    public static class AppHelper
    {
        public static DataAccessSampleConfiguration LoadConfiguration()
        {
            // TODO: load configuration from JSON file! or from app settings
            return CrateSampleTestConfig();
        }

        /// <summary>
        /// Read connection string from app settings.
        /// </summary>
        /// <returns></returns>
        public static string GetDatabaseConnectionString()
        {
            return ConfigurationManager.AppSettings["ConnectionString"];
        }

        public static string GetDefaultConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public static string GetConnectionStringByName(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        private static DataAccessSampleConfiguration CrateSampleTestConfig()
        {
            return new DataAccessSampleConfiguration
            {
                Name = "Test-Default",
                DatabaseConfiguration = new DatabaseConfiguration
                {
                    ConnectionString = @"data source=DIGIMAN-PC-2\DIGIMANDB;initial catalog=CofeePauseDatabase;integrated security=True;",
                    CommandTimeout = 60
                }
            };
        }
    }
}
