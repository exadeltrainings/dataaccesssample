﻿using DataAccessSample.Common.Configuration;

namespace DataAccessSample.ConsoleApp
{
    public static class AppData
    {
        public static DataAccessSampleConfiguration Configuration { get; set; }
    }
}
