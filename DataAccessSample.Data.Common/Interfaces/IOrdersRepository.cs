﻿using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;

namespace DataAccessSample.Data.Common.Interfaces
{
    public interface IOrdersRepository
    {
        Order Get(int id);
        IEnumerable<Order> GetAll();
        Order Add(Order model);
        void Update(Order model);
        void Remove(int id);
    }
}
