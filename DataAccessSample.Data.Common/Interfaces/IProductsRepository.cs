﻿using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;

namespace DataAccessSample.Data.Common.Interfaces
{
    public interface IProductsRepository
    {
        Product Get(int id);
        IEnumerable<Product> GetAll();
        Product Add(Product model);
        void Update(Product model);
        void Remove(int id);
    }
}
