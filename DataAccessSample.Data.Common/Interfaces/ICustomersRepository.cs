﻿using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;

namespace DataAccessSample.Data.Common.Interfaces
{
    public interface ICustomersRepository
    {
        Customer Get(int id);
        IEnumerable<Customer> GetAll();
        Customer Add(Customer model);
        void Update(Customer model);
        void Remove(int id);
    }
}
