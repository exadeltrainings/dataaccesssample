﻿namespace DataAccessSample.Data.Common.Interfaces
{
    public interface IDataFactory
    {
        IOrdersRepository CreateOrdersRepository();
        ICustomersRepository CreateCustomersRepository();
        IProductsRepository CreateProductsRepository();
    }
}
