﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessSample.Data.Common
{
    public static class DbConvert
    {
        public static string GetStringVal(DataRow dataRow, string columnName)
        {
            if (dataRow[columnName] != DBNull.Value)
                return Convert.ToString(dataRow[columnName]);
            return string.Empty;
        }

        public static int GetIntVal(DataRow dataRow, string columnName)
        {
            if (dataRow[columnName] != DBNull.Value)
                return Convert.ToInt32(dataRow[columnName]);
            return 0;
        }

        public static bool GetBoolVal(DataRow dataRow, string columnName)
        {
            if (dataRow[columnName] != DBNull.Value)
                return Convert.ToBoolean(dataRow[columnName]);
            return false;
        }

        public static string GetStringVal(SqlDataReader reader, string columnName)
        {
            if (reader[columnName] != DBNull.Value)
                return Convert.ToString(reader[columnName]);
            return string.Empty;
        }

        public static int GetIntVal(SqlDataReader reader, string columnName)
        {
            if (reader[columnName] != DBNull.Value)
                return Convert.ToInt32(reader[columnName]);
            return 0;
        }

        public static bool GetBoolVal(SqlDataReader reader, string columnName)
        {
            if (reader[columnName] != DBNull.Value)
                return Convert.ToBoolean(reader[columnName]);
            return false;
        }

        public static string GetStringField(DataRow dataRow, string columnName)
        {
            return DbConvert.GetStringField(dataRow, columnName, string.Empty);
        }

        public static string GetStringField(DataRow dataRow, string columnName, string nullValue)
        {
            if (dataRow[columnName] != DBNull.Value)
                return (string)dataRow[columnName];
            return nullValue;
        }

        public static string GetStringField(IDataRecord dataRecord, string columnName)
        {
            return DbConvert.GetStringField(dataRecord, columnName, string.Empty);
        }

        public static string GetStringField(IDataRecord dataRecord, string columnName, string nullValue)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (string)dataRecord[columnName];
            return nullValue;
        }

        public static string GetNullableString(IDataRecord dataRecord, string columnName)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (string)dataRecord[columnName];
            return (string)null;
        }

        public static string GetNullableString(DataRow row, string columnName)
        {
            if (row[columnName] != DBNull.Value)
                return (string)row[columnName];
            return (string)null;
        }

        public static int GetIntField(DataRow dataRow, string columnName)
        {
            return DbConvert.GetIntField(dataRow, columnName, 0);
        }

        public static int GetIntField(DataRow dataRow, string columnName, int nullValue)
        {
            if (dataRow[columnName] != DBNull.Value)
                return (int)dataRow[columnName];
            return nullValue;
        }

        public static int GetIntField(IDataRecord dataRecord, string columnName)
        {
            return DbConvert.GetIntField(dataRecord, columnName, 0);
        }

        public static int GetIntField(IDataRecord dataRecord, string columnName, int nullValue)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (int)dataRecord[columnName];
            return nullValue;
        }

        public static int? GetNullableInt(IDataRecord dataRecord, string columnName)
        {
            int? nullable = new int?();
            if (dataRecord[columnName] != DBNull.Value)
                nullable = new int?((int)dataRecord[columnName]);
            return nullable;
        }

        public static int? GetNullableInt(DataRow row, string columnName)
        {
            int? nullable = new int?();
            if (row[columnName] != DBNull.Value)
                nullable = new int?((int)row[columnName]);
            return nullable;
        }

        public static bool GetBoolField(DataRow dataRow, string columnName)
        {
            return DbConvert.GetBoolField(dataRow, columnName, false);
        }

        public static bool GetBoolField(DataRow dataRow, string columnName, bool nullValue)
        {
            if (dataRow[columnName] != DBNull.Value)
                return (bool)dataRow[columnName];
            return nullValue;
        }

        public static bool GetBoolField(IDataRecord dataRecord, string columnName)
        {
            return DbConvert.GetBoolField(dataRecord, columnName, false);
        }

        public static bool GetBoolField(IDataRecord dataRecord, string columnName, bool nullValue)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return Convert.ToBoolean(dataRecord[columnName]);
            return nullValue;
        }

        public static bool? GetNullableBool(IDataRecord dataRecord, string columnName)
        {
            bool? nullable = new bool?();
            if (dataRecord[columnName] != DBNull.Value)
                nullable = new bool?((bool)dataRecord[columnName]);
            return nullable;
        }

        public static bool? GetNullableBool(DataRow row, string columnName)
        {
            bool? nullable = new bool?();
            if (row[columnName] != DBNull.Value)
                nullable = new bool?((bool)row[columnName]);
            return nullable;
        }

        public static DateTime GetDateField(DataRow row, string columnName)
        {
            if (row[columnName] != DBNull.Value)
                return (DateTime)row[columnName];
            throw new DbHelperException("Cannot get value for null date");
        }

        public static DateTime GetDateField(DataRow row, string columnName, DateTime nullValue)
        {
            if (row[columnName] != DBNull.Value)
                return (DateTime)row[columnName];
            return nullValue;
        }

        public static DateTime GetDateField(IDataRecord dataRecord, string columnName)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (DateTime)dataRecord[columnName];
            throw new DbHelperException("Cannot get value for null date");
        }

        public static DateTime GetDateField(IDataRecord dataRecord, string columnName, DateTime nullValue)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (DateTime)dataRecord[columnName];
            return nullValue;
        }

        public static DateTime? GetNullableDate(IDataRecord dataRecord, string columnName)
        {
            DateTime? nullable = new DateTime?();
            if (dataRecord[columnName] != DBNull.Value)
                nullable = new DateTime?((DateTime)dataRecord[columnName]);
            return nullable;
        }

        public static DateTime? GetNullableDate(DataRow row, string columnName)
        {
            DateTime? nullable = new DateTime?();
            if (row[columnName] != DBNull.Value)
                nullable = new DateTime?((DateTime)row[columnName]);
            return nullable;
        }

        public static double GetDoubleField(DataRow dataRow, string columnName)
        {
            return DbConvert.GetDoubleField(dataRow, columnName, 0.0);
        }

        public static double GetDoubleField(DataRow dataRow, string columnName, double nullValue)
        {
            if (dataRow[columnName] != DBNull.Value)
                return (double)(float)dataRow[columnName];
            return nullValue;
        }

        public static double GetDoubleField(IDataRecord dataRecord, string columnName)
        {
            return DbConvert.GetDoubleField(dataRecord, columnName, 0.0);
        }

        public static double GetDoubleField(IDataRecord dataRecord, string columnName, double nullValue)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (double)dataRecord[columnName];
            return nullValue;
        }

        public static double? GetNullableDouble(IDataRecord dataRecord, string columnName)
        {
            double? nullable = new double?();
            if (dataRecord[columnName] != DBNull.Value)
                nullable = new double?((double)dataRecord[columnName]);
            return nullable;
        }

        public static double? GetNullableDouble(DataRow row, string columnName)
        {
            double? nullable = new double?();
            if (row[columnName] != DBNull.Value)
                nullable = new double?((double)row[columnName]);
            return nullable;
        }

        public static Guid GetGuidField(IDataRecord dataRecord, string columnName)
        {
            var value = new Guid();
            if (dataRecord[columnName] != DBNull.Value)
                value = (Guid)dataRecord[columnName];
            return value;
        }

        public static decimal GetDecimalField(IDataRecord dataRecord, string columnName, decimal nullValue = (decimal)0.0)
        {
            if (dataRecord[columnName] != DBNull.Value)
                return (decimal)dataRecord[columnName];
            return nullValue;
        }
    }
}
