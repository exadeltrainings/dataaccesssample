﻿using System;

namespace DataAccessSample.Data.Common
{
    public class DbHelperException : Exception
    {
        public DbHelperException(string message) : base(message)
        {
        }
    }
}