﻿namespace DataAccessSample.Data.Common.DataObjects
{
    public class Customer : BaseDataObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
    }
}
