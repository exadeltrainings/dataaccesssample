﻿using System;

namespace DataAccessSample.Data.Common.DataObjects
{
    public class Order : BaseDataObject
    {
        public int CustomerId { get; set; }
        public int Amount { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
