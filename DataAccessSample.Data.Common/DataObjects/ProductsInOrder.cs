﻿namespace DataAccessSample.Data.Common.DataObjects
{
    public class ProductsInOrder : BaseDataObject
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
    }
}
