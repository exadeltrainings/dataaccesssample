﻿namespace DataAccessSample.Data.Common.DataObjects
{
    public class Product : BaseDataObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
    }
}
