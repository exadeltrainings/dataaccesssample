﻿namespace DataAccessSample.Data.Common.DataObjects
{
    public class BaseDataObject
    {
        public int Id { get; set; }
    }
}
