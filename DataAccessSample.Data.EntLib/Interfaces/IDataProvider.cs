﻿using System.Data;
using DataAccessSample.Data.EntLib.DataProviders;

namespace DataAccessSample.Data.EntLib.Interfaces
{
    public interface IDataProvider
    {
        string ConnectionString { get; }

        int CommandTimeOut { get; }

        IDataReader ExecuteStoredProc(string storedProcName, ParamInfo[] parameters);

        IDataReader ExecuteQuery(string queryText, ParamInfo[] parameters);

        void ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters);

        void ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out int identity);

        void ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out object feedback);

        void ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out object feedback, ParameterDirection paramDirection);

        void ExecuteNonQueryQuery(string queryText, ParamInfo[] parameters);
    }
}
