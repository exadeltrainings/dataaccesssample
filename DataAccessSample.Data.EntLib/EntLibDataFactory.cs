﻿using DataAccessSample.Data.Common.Interfaces;
using DataAccessSample.Data.EntLib.DataProviders;
using DataAccessSample.Data.EntLib.Interfaces;
using DataAccessSample.Data.EntLib.Repositories;

namespace DataAccessSample.Data.EntLib
{
    public class EntLibDataFactory : IDataFactory
    {
        private readonly string _connectionString;
        private readonly int _commandTimeout;
        private static IDataProvider _dataProvider;

        public EntLibDataFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        private IDataProvider CreateDataProvider()
        {
            return _dataProvider ?? (_dataProvider = new EnterpriseLibraryDataProvider(_connectionString));
        }

        public IOrdersRepository CreateOrdersRepository()
        {
            return new OrdersRepository(CreateDataProvider());
        }
        
        public ICustomersRepository CreateCustomersRepository()
        {
            return new CustomersRepository(CreateDataProvider());
        }

        public IProductsRepository CreateProductsRepository()
        {
            return new ProductsRepository(CreateDataProvider());
        }
    }
}
