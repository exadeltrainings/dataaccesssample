﻿using System;
using System.Data;

namespace DataAccessSample.Data.EntLib.DataProviders
{
    public class ParamInfo
    {
        public readonly string Name;
        public readonly DbType Type;
        public object Value;
        public readonly ParameterDirection Direction;
        public readonly int Size;

        public ParamInfo(string name, DbType paramType, object paramValue, ParameterDirection direction, int size)
        {
            this.Name = name;
            this.Type = paramType;
            this.Value = paramValue ?? (object)DBNull.Value;
            this.Direction = direction;
            this.Size = size;
        }

        public ParamInfo(string name, DbType paramType, object paramValue)
        {
            this.Name = name;
            this.Type = paramType;
            this.Value = paramValue ?? (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, DbType paramType)
        {
            this.Name = name;
            this.Type = paramType;
            this.Value = (object)null;
            this.Direction = ParameterDirection.Output;
            this.Size = 0;
        }

        public ParamInfo(string name, string paramValue)
        {
            this.Name = name;
            this.Type = DbType.String;
            this.Value = paramValue != null ? (object)paramValue : (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, int paramValue)
        {
            this.Name = name;
            this.Type = DbType.Int32;
            this.Value = (object)paramValue;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, int? paramValue)
        {
            this.Name = name;
            this.Type = DbType.Int32;
            this.Value = paramValue.HasValue ? (object)paramValue.Value : (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, bool paramValue)
        {
            this.Name = name;
            this.Type = DbType.Boolean;
            this.Value = (object)(paramValue ? 1 : 0);
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, bool? paramValue)
        {
            this.Name = name;
            this.Type = DbType.Boolean;
            this.Value = paramValue.HasValue ? (object)(paramValue.Value ? 1 : 0) : (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, DateTime paramValue)
        {
            this.Name = name;
            this.Type = DbType.DateTime;
            this.Value = (object)paramValue;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, DateTime? paramValue)
        {
            this.Name = name;
            this.Type = DbType.DateTime;
            this.Value = paramValue.HasValue ? (object)paramValue.Value : (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, decimal paramValue)
        {
            this.Name = name;
            this.Type = DbType.Decimal;
            this.Value = (object)paramValue;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }

        public ParamInfo(string name, decimal? paramValue)
        {
            this.Name = name;
            this.Type = DbType.Decimal;
            this.Value = paramValue.HasValue ? (object)paramValue.Value : (object)DBNull.Value;
            this.Direction = ParameterDirection.Input;
            this.Size = 0;
        }
    }
}