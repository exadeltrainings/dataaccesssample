﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using DataAccessSample.Data.Common;
using DataAccessSample.Data.EntLib.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace DataAccessSample.Data.EntLib.DataProviders
{
    /// <summary>
    /// The old DataPrivider that used in the old versions of DbHelper.
    /// </summary>
    public sealed class EnterpriseLibraryDataProvider : IDataProvider
    {
        private const int DefaultCommandTimeout = 60;
        private readonly string _connectionString;
        private readonly int _commandTimeOut;

        string IDataProvider.ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }

        int IDataProvider.CommandTimeOut
        {
            get
            {
                return _commandTimeOut;
            }
        }

        public EnterpriseLibraryDataProvider(string connectionString)
        {
            _connectionString = connectionString;
            _commandTimeOut = DefaultCommandTimeout;
        }

        public EnterpriseLibraryDataProvider(string connectionString, int commandTimeOut)
        {
        }

        private Database GetProviderDatabase()
        {
            return new SqlDatabase(_connectionString);
        }

        IDataReader IDataProvider.ExecuteStoredProc(string storedProcName, ParamInfo[] parameters)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand storedProcCommand = sqlDatabase.GetStoredProcCommand(storedProcName);
            storedProcCommand.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, storedProcCommand, (IEnumerable<ParamInfo>)parameters);
            IDataReader dataReader = sqlDatabase.ExecuteReader(storedProcCommand);
            SetParameters(storedProcCommand, parameters);
            return dataReader;
        }

        IDataReader IDataProvider.ExecuteQuery(string queryText, ParamInfo[] parameters)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand sqlStringCommand = sqlDatabase.GetSqlStringCommand(queryText);
            sqlStringCommand.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, sqlStringCommand, (IEnumerable<ParamInfo>)parameters);
            IDataReader dataReader = sqlDatabase.ExecuteReader(sqlStringCommand);
            SetParameters(sqlStringCommand, parameters);
            return dataReader;
        }

        void IDataProvider.ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand storedProcCommand = sqlDatabase.GetStoredProcCommand(storedProcName);
            storedProcCommand.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, storedProcCommand, (IEnumerable<ParamInfo>)parameters);
            sqlDatabase.ExecuteNonQuery(storedProcCommand);
            SetParameters(storedProcCommand, parameters);
        }

        void IDataProvider.ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out int identity)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand command = sqlDatabase.GetStoredProcCommand(storedProcName);
            command.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, command, (IEnumerable<ParamInfo>)parameters);
            sqlDatabase.ExecuteNonQuery(command);
            identity = Enumerable.FirstOrDefault<int>(Enumerable.Select<ParamInfo, int>(Enumerable.Where<ParamInfo>((IEnumerable<ParamInfo>)parameters, (Func<ParamInfo, bool>)(info =>
            {
                if (info.Direction == ParameterDirection.Output)
                    return info.Type == DbType.Int32;
                return false;
            })), (Func<ParamInfo, int>)(info => (int)command.Parameters[command.Parameters.IndexOf(BuildParameterName(info.Name))].Value)));
            if (identity == 0)
                throw new DbHelperException(string.Format("Object identity is not found (stored proc {0})", (object)storedProcName));
            SetParameters(command, parameters);
        }

        void IDataProvider.ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out object feedback)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand command = sqlDatabase.GetStoredProcCommand(storedProcName);
            command.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, command, (IEnumerable<ParamInfo>)parameters);
            sqlDatabase.ExecuteNonQuery(command);
            feedback = Enumerable.FirstOrDefault<object>(Enumerable.Select<ParamInfo, object>(Enumerable.Where<ParamInfo>((IEnumerable<ParamInfo>)parameters, (Func<ParamInfo, bool>)(info => info.Direction == ParameterDirection.Output)), (Func<ParamInfo, object>)(info => command.Parameters[command.Parameters.IndexOf(BuildParameterName(info.Name))].Value)));
            if (feedback == null)
                throw new DbHelperException(string.Format("Feedback from stored proc is not found (stored proc {0})", (object)storedProcName));
            SetParameters(command, parameters);
        }

        public void ExecuteNonQueryStoredProc(string storedProcName, ParamInfo[] parameters, out object feedback, ParameterDirection paramDirection)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand command = sqlDatabase.GetStoredProcCommand(storedProcName);
            command.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, command, (IEnumerable<ParamInfo>)parameters);
            sqlDatabase.ExecuteNonQuery(command);
            feedback = Enumerable.FirstOrDefault<object>(Enumerable.Select<ParamInfo, object>(Enumerable.Where<ParamInfo>((IEnumerable<ParamInfo>)parameters, (Func<ParamInfo, bool>)(info => info.Direction == paramDirection)), (Func<ParamInfo, object>)(info => command.Parameters[command.Parameters.IndexOf(BuildParameterName(info.Name))].Value)));
            if (feedback == null)
                throw new DbHelperException(string.Format("Feedback from stored proc is not found (stored proc {0})", (object)storedProcName));
            SetParameters(command, parameters);
        }

        void IDataProvider.ExecuteNonQueryQuery(string queryText, ParamInfo[] parameters)
        {
            var sqlDatabase = GetProviderDatabase();
            DbCommand sqlStringCommand = sqlDatabase.GetSqlStringCommand(queryText);
            sqlStringCommand.CommandTimeout = _commandTimeOut;
            LoadParameters((Database)sqlDatabase, sqlStringCommand, (IEnumerable<ParamInfo>)parameters);
            sqlDatabase.ExecuteNonQuery(sqlStringCommand);
            SetParameters(sqlStringCommand, parameters);
        }
        
        #region Helper methods.

        private static void LoadParameters(Database database, DbCommand command, IEnumerable<ParamInfo> parameters)
        {
            if (parameters == null)
                return;
            foreach (ParamInfo paramInfo in parameters)
            {
                switch (paramInfo.Direction)
                {
                    case ParameterDirection.Input:
                        database.AddInParameter(command, paramInfo.Name, paramInfo.Type, paramInfo.Value ?? (object)DBNull.Value);
                        continue;
                    case ParameterDirection.Output:
                        database.AddOutParameter(command, paramInfo.Name, paramInfo.Type, paramInfo.Size);
                        continue;
                    case ParameterDirection.InputOutput:
                        database.AddParameter(command, paramInfo.Name, paramInfo.Type, paramInfo.Size, ParameterDirection.InputOutput, false, (byte)0, (byte)0, string.Empty, DataRowVersion.Default, paramInfo.Value ?? (object)DBNull.Value);
                        continue;
                    case ParameterDirection.ReturnValue:
                        database.AddParameter(command, paramInfo.Name, paramInfo.Type, paramInfo.Size, ParameterDirection.ReturnValue, false, (byte)0, (byte)0, string.Empty, DataRowVersion.Default, paramInfo.Value ?? (object)DBNull.Value);
                        continue;
                    default:
                        continue;
                }
            }
        }

        private static void LoadParameters(DbCommand command, IEnumerable<ParamInfo> parameters)
        {
            if (parameters == null)
                return;
            foreach (ParamInfo paramInfo in parameters)
            {
                switch (paramInfo.Direction)
                {
                    case ParameterDirection.Input:
                        AddParameter(command, paramInfo.Name, paramInfo.Type, 0, ParameterDirection.Input, false, string.Empty, DataRowVersion.Default, paramInfo.Value);
                        continue;
                    case ParameterDirection.Output:
                        AddParameter(command, paramInfo.Name, paramInfo.Type, paramInfo.Size, ParameterDirection.Output, true, string.Empty, DataRowVersion.Default, (object)DBNull.Value);
                        continue;
                    case ParameterDirection.InputOutput:
                        AddParameter(command, paramInfo.Name, paramInfo.Type, 0, ParameterDirection.InputOutput, false, string.Empty, DataRowVersion.Default, paramInfo.Value);
                        continue;
                    case ParameterDirection.ReturnValue:
                        AddParameter(command, paramInfo.Name, paramInfo.Type, 0, ParameterDirection.ReturnValue, false, string.Empty, DataRowVersion.Default, paramInfo.Value);
                        continue;
                    default:
                        continue;
                }
            }
        }

        private static void SetParameters(DbCommand command, ParamInfo[] parameters)
        {
            if (parameters == null)
                return;
            for (int index = 0; index < parameters.Length; ++index)
            {
                ParamInfo paramInfo = parameters[index];
                DbParameter dbParameter = command.Parameters[BuildParameterName(paramInfo.Name)];
                switch (dbParameter.Direction)
                {
                    case ParameterDirection.Output:
                    case ParameterDirection.InputOutput:
                    case ParameterDirection.ReturnValue:
                        parameters[index].Value = dbParameter.Value;
                        break;
                }
            }
        }

        private static DbCommand CreateCommand(DbConnection connection, DbTransaction transaction, CommandType aType, string aText, int aCommandTimeOut)
        {
            DbCommand command = connection.CreateCommand();
            command.CommandTimeout = aCommandTimeOut;
            command.Transaction = transaction;
            command.CommandType = aType;
            command.CommandText = aText;
            return command;
        }

        private static void AddParameter(DbCommand command, string aName, DbType aType, int aSize, ParameterDirection aDirection, bool aIsNullable, string aSourceColumn, DataRowVersion aSourceVersion, object aValue)
        {
            DbParameter parameter = command.CreateParameter();
            parameter.ParameterName = BuildParameterName(aName);
            parameter.DbType = aType;
            parameter.Size = aSize;
            parameter.Value = aValue ?? (object)DBNull.Value;
            parameter.Direction = aDirection;
            parameter.IsNullable = aIsNullable;
            parameter.SourceColumn = aSourceColumn;
            parameter.SourceVersion = aSourceVersion;
            command.Parameters.Add((object)parameter);
        }

        private static string BuildParameterName(string name)
        {
            if (name.Length > 0 && !name[0].ToString().Equals("@"))
                return name.Insert(0, "@");
            return name;
        }

        #endregion
    }
}
