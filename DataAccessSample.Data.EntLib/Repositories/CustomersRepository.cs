﻿using System;
using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;
using DataAccessSample.Data.EntLib.Interfaces;

namespace DataAccessSample.Data.EntLib.Repositories
{
    public class CustomersRepository : RepositoryBase, ICustomersRepository
    {
        public CustomersRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public Customer Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> GetAll()
        {
            throw new NotImplementedException();
        }

        public Customer Add(Customer model)
        {
            throw new NotImplementedException();
        }

        public void Update(Customer model)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
