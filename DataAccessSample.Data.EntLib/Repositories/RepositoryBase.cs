﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using DataAccessSample.Data.EntLib.DataProviders;
using DataAccessSample.Data.EntLib.Interfaces;

namespace DataAccessSample.Data.EntLib.Repositories
{
    public class RepositoryBase
    {
        protected IDataProvider DataProvider { get; set; }

        public RepositoryBase(IDataProvider dataProvider)
        {
            DataProvider = dataProvider;
        }

        protected void RemoveRecord(string procedureName, ParamInfo[] parameters)
        {
            DataProvider.ExecuteNonQueryStoredProc(procedureName, parameters);
        }

        protected List<TResult> LoadList<TResult>(string procedureName, ParamInfo[] parameters,
            Func<IDataReader, TResult> loadObjectAction)
        {
            var result = new List<TResult>();
            using (var reader = DataProvider.ExecuteStoredProc(procedureName, parameters))
            {
                while (reader.Read())
                {
                    result.Add(loadObjectAction(reader));
                }
            }
            return result;
        }

        protected void AddRecord<T>(T entity, string procedureName, ParamInfo[] parameters, string idPropertyName)
        {
            var reader = DataProvider.ExecuteStoredProc(procedureName, parameters);

            using (reader)
            {
                if (!string.IsNullOrEmpty(idPropertyName) && reader.Read())
                {
                    SetPropertyValue(entity, idPropertyName, reader.GetInt32(0));
                }
            }
        }

        protected void UpdateRecord(string procedureName, ParamInfo[] parameters)
        {
            DataProvider.ExecuteNonQueryStoredProc(procedureName, parameters);
        }

        protected TResult GetRecord<TResult>(string procedureName, ParamInfo[] parameters,
            Func<IDataReader, TResult> loadObjectAction)
        {

            TResult result = default(TResult);

            var reader = DataProvider.ExecuteStoredProc(procedureName, parameters);

            using (reader)
            {
                if (reader.Read())
                {
                    result = loadObjectAction(reader);
                }
            }

            return result;
        }

        protected void ExecuteStoredProcedureWithoutParams(string procedureName)
        {
            DataProvider.ExecuteNonQueryStoredProc(procedureName, (ParamInfo[]) null);
        }

        protected void ExecuteStoredProcedureWithParams(string procedureName, ParamInfo[] parameters)
        {
            DataProvider.ExecuteNonQueryStoredProc(procedureName, parameters);
        }

        private void SetPropertyValue(object instance, string propertyName, object value)
        {
            if (instance != null && !string.IsNullOrEmpty(propertyName))
            {
                Type instanceType = instance.GetType();
                PropertyInfo propertyInfo = instanceType.GetProperty(propertyName);
                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(instance, value, null);
                }
            }
        }
    }
}
