﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccessSample.Data.Common;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;
using DataAccessSample.Data.EntLib.DataProviders;
using DataAccessSample.Data.EntLib.Interfaces;

namespace DataAccessSample.Data.EntLib.Repositories
{
    // TODO: create the new stored procedure in database and modify code if needed!

    public class ProductsRepository : RepositoryBase, IProductsRepository
    {
        private const string ProductsAdd = "[dbo].[procProducts_Add]";
        private const string ProductsUpdate = "[dbo].[procProducts_Update]";
        private const string ProductsRemove = "[dbo].[procProducts_Remove]";
        private const string ProductsGetAll = "[dbo].[procProducts_GetAll]";
        private const string ProductsGet = "[dbo].[procProducts_Get]";

        public ProductsRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public Product Get(int id)
        {
            var parameters = new[]
            {
                new ParamInfo("Id", id)
            };

            return GetRecord(ProductsGet, parameters, LoadProduct);
        }

        public IEnumerable<Product> GetAll()
        {
            return LoadList(ProductsGetAll, null, LoadProduct);
        }

        public Product Add(Product model)
        {
            var parameters = BuildParameters(model, true);

            AddRecord(model, ProductsAdd, parameters, "Id");

            return model;
        }

        public void Update(Product model)
        {
            var parameters = BuildParameters(model, false);

            UpdateRecord(ProductsUpdate, parameters);
        }

        public void Remove(int id)
        {
            var parameters = new[]
            {
                new ParamInfo("ProductId", id)
            };

            RemoveRecord(ProductsRemove, parameters);
        }

        private Product LoadProduct(IDataReader reader)
        {
            return new Product
            {
                Id = DbConvert.GetIntField(reader, "Id"),
                Name = DbConvert.GetStringField(reader, "Name"),
                Description = DbConvert.GetStringField(reader, "Description"),
                Cost = DbConvert.GetDecimalField(reader, "Cost")
            };
        }

        private ParamInfo[] BuildParameters(Product model, bool isNewRecord)
        {
            if (isNewRecord)
            {
                return new[]
                {
                    new ParamInfo("Name", model.Name),
                    new ParamInfo("Description", model.Description),
                    new ParamInfo("Cost", model.Cost),
                };
            }
            return new[]
            {
                new ParamInfo("Id", model.Id),
                new ParamInfo("Name", model.Name),
                new ParamInfo("Description", model.Description),
                new ParamInfo("Cost", model.Cost)
            };
        }
    }
}
