﻿using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.EntityFramework
{
    public class EntityFrameworkDataFactory : IDataFactory
    {
        private readonly string _connectionString;

        public EntityFrameworkDataFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IOrdersRepository CreateOrdersRepository()
        {
            throw new System.NotImplementedException();
        }

        public ICustomersRepository CreateCustomersRepository()
        {
            throw new System.NotImplementedException();
        }

        public IProductsRepository CreateProductsRepository()
        {
            throw new System.NotImplementedException();
        }
    }
}
