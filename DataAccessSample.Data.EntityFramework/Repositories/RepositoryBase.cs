﻿namespace DataAccessSample.Data.EntityFramework.Repositories
{
    public class RepositoryBase
    {
        protected string ConnectionString { get; set; }

        public RepositoryBase(string connectionString)
        {
            ConnectionString = connectionString;
        }
    }
}
