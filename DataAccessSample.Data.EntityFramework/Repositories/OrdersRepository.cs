﻿using System;
using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.EntityFramework.Repositories
{
    public class OrdersRepository : RepositoryBase, IOrdersRepository
    {
        public OrdersRepository(string connectionString) : base(connectionString)
        {
        }

        public Order Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> GetAll()
        {
            throw new NotImplementedException();
        }

        public Order Add(Order model)
        {
            throw new NotImplementedException();
        }

        public void Update(Order model)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
