﻿using System;
using System.Collections.Generic;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.EntityFramework.Repositories
{
    public class ProductsRepository : RepositoryBase, IProductsRepository
    {
        public ProductsRepository(string connectionString) : base(connectionString)
        {
        }

        public Product Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
            throw new NotImplementedException();
        }

        public Product Add(Product model)
        {
            throw new NotImplementedException();
        }

        public void Update(Product model)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
