﻿namespace DataAccessSample.Common.Configuration
{
    public class DataAccessSampleConfiguration
    {
        public string Name { get; set; }
        public DatabaseConfiguration DatabaseConfiguration { get; set; }
    }

    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }
        public int CommandTimeout { get; set; }
    }
}
