﻿using DataAccessSample.Data.Common.DataObjects;

namespace DataAccessSample.Common.Helpers
{
    public static class DatabaseDataGenerator
    {
        public static Customer CreateCustomer()
        {
            return new Customer();
        }

        public static Product CreateProduct()
        {
            return new Product();
        }

        public static Order CreateOrder()
        {
            return new Order();
        }
    }
}
