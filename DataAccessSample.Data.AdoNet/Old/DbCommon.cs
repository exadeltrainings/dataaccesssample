﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataAccessSample.Data.AdoNet.Old
{
    /// <summary>
    /// Класс для общих функций доступа к БД и работы с ней
    /// Provide classic mode for working with DB 
    /// </summary>
    public class DbCommon
    {
        // строка подключения к БД
        private readonly string _connstr; // строка подключения

        /// <summary>
        /// Инициализация класса для работы с БД посредством SQL запросов
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        public DbCommon(string connectionString)
        {
            _connstr = connectionString;
        }

        /// <summary>
        /// Выполнение запроса на выборку (SELECT)
        /// </summary>
        /// <param name="query">Строка запроса</param>
        /// <returns>Возвращает таблицу с данными</returns>
        public DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable();
            string error = "";
            using (SqlConnection sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // выборка данных по запросу
                        SqlCommand com = new SqlCommand(query, sqlconn);
                        SqlDataAdapter adapter = new SqlDataAdapter(com);
                        DataSet ds = new DataSet();
                        adapter.Fill(ds); // заполнение таблицы с данными данными из БД
                        dt = ds.Tables[0]; // выбираем первую таблицу, так как по запросу больше не должно быть
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
            return dt;
        }

        /// <summary>
        /// Выполнение запроса к хранимой процедуре с множественным возвратом данных
        /// </summary>
        /// <param name="procedureName">Название процедуры</param>
        /// <param name="parameters">Список параметров процедуры</param>
        /// <returns>Возвращает набор данных из результаты выполнения запроса</returns>
        public DataSet ExecuteMultipleQuery(string procedureName, ref List<SqlParameter> parameters)
        {
            var ds = new DataSet();
            string error = "";
            using (var sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // выборка данных по запросу
                        var com = GenerateCommand(procedureName, parameters, sqlconn);
                        var adapter = new SqlDataAdapter(com);
                        adapter.Fill(ds); // заполнение таблицы с данными данными из БД
                        parameters = ConvertToList(com.Parameters);
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
            return ds;
        }

        /// <summary>
        /// Выполнение запроса к хранимой процедуре с множественным возвратом данных
        /// </summary>
        /// <param name="procedureName">Название процедуры</param>
        /// <param name="parameters">Список параметров процедуры</param>
        public void ExecuteMultipleNonQuery(string procedureName, ref List<SqlParameter> parameters)
        {
            var ds = new DataSet();
            string error = "";
            using (var sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // выборка данных по запросу
                        var com = GenerateCommand(procedureName, parameters, sqlconn);
                        com.ExecuteNonQuery();
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
        }

        /// <summary>
        /// Выполнение запроса к хранимой процедуре
        /// </summary>
        /// <param name="procedureName">Название процедуры</param>
        /// <param name="parameters">Список параметров процедуры</param>
        public object ExecuteMultipleScalarQuery(string procedureName, ref List<SqlParameter> parameters)
        {
            var ds = new DataSet();
            string error = "";
            object result = null;
            using (var sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // выборка данных по запросу
                        var com = GenerateCommand(procedureName, parameters, sqlconn);
                        result = com.ExecuteScalar();
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
            return result;
        }

        /// <summary>
        /// Выполнение команды SQL Insert, Update, Delete
        /// </summary>
        /// <param name="query">Строка запроса SQL</param>
        /// <returns>Возвращает количество обработанных строк</returns>
        public int ExecuteNonQuery(string query)
        {
            string error = "";
            int resultString = 0;
            using (SqlConnection sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // исполнение запроса
                        SqlCommand com = new SqlCommand(query, sqlconn);
                        resultString = com.ExecuteNonQuery();
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
            return resultString;
        }

        /// <summary>
        /// Выполнение команды SQL Insert, Update, Delete
        /// </summary>
        /// <param name="query">Строка запроса SQL</param>
        /// <param name="notification">Возвращает или задает значение, определяющее объект SqlNotificationRequest, связанный с выполняемой командой.</param>
        /// <returns>Возвращает количество обработанных строк</returns>
        public int ExecuteNonQuery2(string query, ref string notification)
        {
            string error = "";
            int resultString = 0;
            using (SqlConnection sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // исполнение запроса
                        SqlCommand com = new SqlCommand(query, sqlconn);
                        resultString = com.ExecuteNonQuery();
                        notification = com.Notification.ToString();
                    }
                    catch (Exception e2) // ошибка выполнения SQLкоманды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
            return resultString;
        }

        /// <summary>
        /// Выполнение хранимой процедуры с параметрами
        /// </summary>
        /// <param name="procedureName">Название хранимой процедуры</param>
        /// <param name="parameters">Список параметров</param>
        /// <param name="isSelect">Если должен быть возврат данных от хранимой процедуры</param>
        public void ExecuteProcedure(string procedureName, Dictionary<string, object> parameters, bool isSelect, out DataTable dt)
        {
            dt = new DataTable();
            string error = "";
            using (SqlConnection sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // формирование команды SQL на исполнение
                        SqlCommand com = GenerateCommand(procedureName, parameters, sqlconn);

                        if (isSelect)
                        {
                            SqlDataAdapter adapter = new SqlDataAdapter(com);
                            DataSet ds = new DataSet();
                            adapter.Fill(ds); // заполнение таблицы с данными данными из БД
                            dt = ds.Tables[0]; // выбираем первую таблицу, так как по запросу больше не должно быть
                        }
                        else
                        {
                            // исполнение запроса (Insert, Delete, Update)
                            com.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e2) // ошибка выполнения SQL команды
                    {
                        error = String.Format("Error on sql command execute. {0}", e2.Message);
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = String.Format("Error on sql connection. {0}", e1.Message);
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
        }

        /// <summary>
        /// Исполнение хранимой процедуры полностью через параматры
        /// </summary>
        /// <param name="procedureName">Название хранимой процедуры</param>
        /// <param name="parameters">Список параметров</param>
        public void ExecuteProcedure(string procedureName, ref List<SqlParameter> parameters)
        {
            string error = "";
            using (SqlConnection sqlconn = new SqlConnection(_connstr))
            {
                try
                {
                    sqlconn.Open(); // открытие соединения с БД
                    try
                    {
                        // формирвоание команды SQL на исполнение
                        SqlCommand com = GenerateCommand(procedureName, parameters, sqlconn);
                        int tmp = com.ExecuteNonQuery();
                        parameters = ConvertToList(com.Parameters);
                    }
                    catch (Exception e2) // ошбка выполнения SQL команды
                    {
                        error = "Error at execte SQL command!" + e2.Message;
                    }
                }
                catch (Exception e1) // ошибка подключения к БД
                {
                    error = "Connection error: " + e1.Message;
                }
                finally
                {
                    sqlconn.Close(); // закрытие соединения
                }
            }
        }

        /// <summary>
        /// Конвертация параметров SqlParameters в список
        /// </summary>
        /// <param name="parameters">Коллекция параметров</param>
        /// <returns>Возвращает параметры в виде списка</returns>
        private List<SqlParameter> ConvertToList(SqlParameterCollection parameters)
        {
            List<SqlParameter> pars = new List<SqlParameter>(parameters.Count);

            pars.AddRange(parameters.Cast<SqlParameter>());

            return pars;
        }

        /// <summary>
        /// Формирование команды для исполнения хранимой процедуры
        /// </summary>
        /// <param name="procedureName">Название хранимой процедуры</param>
        /// <param name="parameters">Список параметров</param>
        /// <param name="connection">Соединение с БД (из пула)</param>
        /// <returns>Возвращает сформированную команду на исполнение</returns>
        private SqlCommand GenerateCommand(string procedureName, Dictionary<string, object> parameters, SqlConnection connection)
        {
            SqlCommand cmd = new SqlCommand(procedureName, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            // формирование списка параметров с их значениями
            foreach (var par in parameters)
            {
                cmd.Parameters.AddWithValue(String.Format("@{0}", par.Key), par.Value);
            }
            return cmd;
        }

        /// <summary>
        /// Формирование списка параметров запроса
        /// </summary>
        /// <param name="procedureName">Название процедуры хранимой</param>
        /// <param name="parameters">Список параметров</param>
        /// <param name="connection">Строка подключения</param>
        /// <returns>Возвращает сформированную команду для выполнения</returns>
        private SqlCommand GenerateCommand(string procedureName, List<SqlParameter> parameters, SqlConnection connection)
        {
            SqlCommand cmd = new SqlCommand(procedureName, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            // формироване списка параметров с их значениями
            foreach (var par in parameters)
            {
                cmd.Parameters.Add(par);
            }
            return cmd;
        }

        /// <summary>
        /// Удаление данных из таблиц
        /// </summary>
        /// <param name="tableNames">Названия таблиц (массив) для очистки</param>
        public void ClearTables(string[] tableNames)
        {
            string query = "";
            for (int i = 0; i < tableNames.Length; i++)
                query += String.Format("DELETE FROM [{0}]; ", tableNames[i]);
            ExecuteNonQuery(query);
        }

        /// <summary>
        /// Удаление таблиц из БД
        /// </summary>
        /// <param name="tableNames">Названия таблиц (массив) для удаления</param>
        public void DeleteTables(string[] tableNames)
        {
            string query = "DROP TABLE";
            for (int i = 0; i < tableNames.Length; i++)
                if (i != tableNames.Length - 1)
                    query += String.Format(" [{0}],", tableNames[i]);
                else
                    query += String.Format(" [{0}];", tableNames[i]);
            ExecuteNonQuery(query);
        }
    }
}
