﻿using DataAccessSample.Data.AdoNet.DataProviders;
using DataAccessSample.Data.AdoNet.Interfaces;
using DataAccessSample.Data.AdoNet.Repositories;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.AdoNet
{
    /// <summary>
    /// Simple factory class to get implementations of the repository to work with database through ADO.NET. 
    /// </summary>
    public class AdoNetDataFactory : IDataFactory
    {
        private readonly string _connectionString;
        private readonly int _commandTimeout;
        private static IDataProvider _dataProvider;

        public AdoNetDataFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public AdoNetDataFactory(string connectionString, int commandTimeout)
        {
            _connectionString = connectionString;
            _commandTimeout = commandTimeout;
        }

        private IDataProvider CreateDataProvider()
        {
            return _dataProvider ?? (_dataProvider = new AdoNetDataProvider(_connectionString));
        }

        public IOrdersRepository CreateOrdersRepository()
        {
            return new OrdersRepository(CreateDataProvider());
        }

        public ICustomersRepository CreateCustomersRepository()
        {
           return new CustomersRepository(CreateDataProvider());
        }

        public IProductsRepository CreateProductsRepository()
        {
            return new ProductsRepository(CreateDataProvider());
        }
    }
}
