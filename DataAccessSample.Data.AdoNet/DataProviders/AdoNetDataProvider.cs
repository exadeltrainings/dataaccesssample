﻿using System.Collections.Generic;
using System.Data;
using DataAccessSample.Data.AdoNet.Interfaces;

namespace DataAccessSample.Data.AdoNet.DataProviders
{
    public class AdoNetDataProvider : IDataProvider
    {
        private const int DefaultCommandTimeout = 60;

        private readonly string _connectionString;
        private readonly int _commandTimeout;

        public AdoNetDataProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public AdoNetDataProvider(string connectionString, int commandTimeout)
        {
            _connectionString = connectionString;
            _commandTimeout = commandTimeout;
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        public int CommandTimeout
        {
            get { return _commandTimeout != 0 ? _commandTimeout : DefaultCommandTimeout; }
        }

        public IEnumerable<IDataRecord> ExecuteQuery(string queryText, object parameters = null)
        {
            var database = new Database(ConnectionString);
            var result = database.Query(queryText, parameters, CommandTimeout);
            return result;
        }

        public IEnumerable<IDataRecord> ExecuteProcedure(string procedureName, object parameters = null)
        {
            var database = new Database(ConnectionString);
            var result = database.Procedure(procedureName, parameters, CommandTimeout);
            return result;
        }

        public T ExecuteScalar<T>(string queryText, object parameters = null)
        {
            var database = new Database(ConnectionString);
            var result = database.Scalar<T>(queryText, parameters, CommandTimeout);
            return result;
        }

        public int ExecuteExec(string queryText, object parameters = null)
        {
            var database = new Database(ConnectionString);
            var rowsAffected = database.Exec(queryText, parameters, CommandTimeout);
            return rowsAffected;
        }
    }
}
