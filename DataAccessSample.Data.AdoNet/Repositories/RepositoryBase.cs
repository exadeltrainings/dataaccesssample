﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataAccessSample.Data.AdoNet.Interfaces;

namespace DataAccessSample.Data.AdoNet.Repositories
{
    public class RepositoryBase
    {
        protected IDataProvider DataProvider { get; set; }

        public RepositoryBase(IDataProvider dataProvider)
        {
            DataProvider = dataProvider;
        }

        protected TResult GetRecord<TResult>(string queryString, Dictionary<string, object> parameters,
            Func<IDataRecord, TResult> loadObjectAction, bool isProcedure)
        {
            TResult result = default(TResult);

            var records = isProcedure
                ? DataProvider.ExecuteQuery(queryString, parameters)
                : DataProvider.ExecuteProcedure(queryString, parameters);

            if (records.Any())
            {
                result = loadObjectAction(records.First());
            }

            return result;
        }
        
        protected List<TResult> LoadList<TResult>(string queryString, Dictionary<string, object> parameters,
            Func<IDataRecord, TResult> loadObjectAction, bool isProcedure)
        {
            var result = new List<TResult>();

            var records = isProcedure
                ? DataProvider.ExecuteQuery(queryString, parameters)
                : DataProvider.ExecuteProcedure(queryString, parameters);

            foreach (var record in records)
            {
                result.Add(loadObjectAction(record));
            }

            return result;
        }
    }
}
