﻿using System.Collections.Generic;
using DataAccessSample.Data.AdoNet.Interfaces;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.AdoNet.Repositories
{
    public class OrdersRepository : RepositoryBase, IOrdersRepository
    {
        public OrdersRepository(IDataProvider dataProvider) : base(dataProvider)
        { }

        public Order Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Order> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Order Add(Order model)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Order model)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
