﻿using System.Collections.Generic;
using System.Data;
using DataAccessSample.Data.AdoNet.Interfaces;
using DataAccessSample.Data.Common;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.AdoNet.Repositories
{
    public class ProductsRepository : RepositoryBase, IProductsRepository
    {
        private const string ProductsGetAll = "[dbo].[procProducts_GetAll]";

        public ProductsRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public Product Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
            return LoadList(ProductsGetAll, null, LoadProduct, true);
        }

        public Product Add(Product model)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Product model)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new System.NotImplementedException();
        }

        private Product LoadProduct(IDataRecord reader)
        {
            return new Product
            {
                Id = DbConvert.GetIntField(reader, "Id"),
                Name = DbConvert.GetStringField(reader, "Name"),
                Description = DbConvert.GetStringField(reader, "Description"),
                Cost = DbConvert.GetDecimalField(reader, "Cost")
            };
        }
    }
}
