﻿using System.Collections.Generic;
using DataAccessSample.Data.AdoNet.Interfaces;
using DataAccessSample.Data.Common.DataObjects;
using DataAccessSample.Data.Common.Interfaces;

namespace DataAccessSample.Data.AdoNet.Repositories
{
    public class CustomersRepository : RepositoryBase, ICustomersRepository
    {
        public CustomersRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public Customer Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Customer> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Customer Add(Customer model)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Customer model)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
