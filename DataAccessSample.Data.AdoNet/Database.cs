﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using DataAccessSample.Data.AdoNet.Interfaces;

namespace DataAccessSample.Data.AdoNet
{
    // see more: https://github.com/gimmi/adoutils - code gets from this repository!

    public class Database : IDatabase
    {
        private readonly DbProviderFactory _factory;
        private readonly string _connectionString;
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private bool _nestedTransactionRollback;

        public Database(string connectionString) : this(connectionString, DbProviderFactories.GetFactory("System.Data.SqlClient")) { }

        public Database(string connStr, DbProviderFactory factory)
        {
            _factory = factory;
            _connectionString = connStr;
        }

        public virtual IConnection OpenConnection()
        {
            if (_connection != null)
            {
                return new Connection();
            }
            DbConnection conn = _factory.CreateConnection();
            conn.ConnectionString = _connectionString;
            conn.Open();
            _connection = conn;
            return new Connection(CloseConnection);
        }

        private void CloseConnection()
        {
            if (_connection != null)
            {
                IDbConnection conn = _connection;
                _connection = null;
                conn.Close();
            }
        }

        public virtual ITransaction BeginTransaction()
        {
            IConnection connection = OpenConnection();
            if (_transaction != null)
            {
                return new Transaction(connection, delegate { }, NotifyNestedTransactionRollback);
            }
            _transaction = _connection.BeginTransaction();
            _nestedTransactionRollback = false;
            return new Transaction(connection, CommitTransaction, RollbackTransaction);
        }

        private void NotifyNestedTransactionRollback()
        {
            _nestedTransactionRollback = true;
        }

        private void CommitTransaction()
        {
            if (_transaction != null)
            {
                if (_nestedTransactionRollback)
                {
                    RollbackTransaction();
                    throw new DataException("Cannot commit transaction when one of the nested transaction has been rolled back");
                }
                IDbTransaction tr = _transaction;
                _transaction = null;
                tr.Commit();
            }
        }

        private void RollbackTransaction()
        {
            if (_transaction != null)
            {
                IDbTransaction tr = _transaction;
                _transaction = null;
                tr.Rollback();
            }
        }

        public virtual T Scalar<T>(string sql, object parameters = null, int? timeout = null)
        {
            using (var cmd = CreateCommand(timeout))
            {
                cmd.DbCommand.CommandText = sql;
                AddParameters(cmd.DbCommand, ToDictionary(parameters));
                object res = cmd.DbCommand.ExecuteScalar();
                return DbFieldConversionUtils.Convert<T>(res);
            }
        }

        public virtual ICommand CreateCommand(int? timeout = null)
        {
            IConnection conn = OpenConnection();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            if (timeout.HasValue)
            {
                cmd.CommandTimeout = timeout.Value;
            }
            return new Command(cmd, conn);
        }

        public virtual IEnumerable<IDataRecord> Query(string sql, object parameters = null, int? timeout = null)
        {
            using (var cmd = CreateCommand(timeout))
            {
                cmd.DbCommand.CommandText = sql;
                AddParameters(cmd.DbCommand, ToDictionary(parameters));
                using (IDataReader rdr = cmd.DbCommand.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        yield return rdr;
                    }
                }
            }
        }

        public virtual IEnumerable<IDataRecord> Procedure(string procedureName, object parameters = null, int? timeout = null)
        {
            using (var cmd = CreateCommand(timeout))
            {
                cmd.DbCommand.CommandType = CommandType.StoredProcedure;
                cmd.DbCommand.CommandText = procedureName;
                AddParameters(cmd.DbCommand, ToDictionary(parameters));
                using (IDataReader rdr = cmd.DbCommand.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        yield return rdr;
                    }
                }
            }
        }

        public virtual int Exec(string sql, object parameters = null, int? timeout = null)
        {
            using (ICommand cmd = CreateCommand(timeout))
            {
                cmd.DbCommand.CommandText = sql;
                AddParameters(cmd.DbCommand, ToDictionary(parameters));
                return cmd.DbCommand.ExecuteNonQuery();
            }
        }

        private static void AddParameters(IDbCommand cmd, IEnumerable<KeyValuePair<string, object>> parameters)
        {
            foreach (var pi in parameters)
            {
                IDataParameter par;
                if (pi.Value is IDataParameter)
                {
                    par = pi.Value as IDataParameter;
                }
                else
                {
                    par = cmd.CreateParameter();
                    par.Value = pi.Value ?? DBNull.Value;
                }
                par.ParameterName = pi.Key;
                cmd.Parameters.Add(par);
            }
        }

        private static IDictionary<string, object> ToDictionary(object o)
        {
            return o as IDictionary<string, object> ?? TypeDescriptor.GetProperties(o).Cast<PropertyDescriptor>().ToDictionary(p => p.Name, p => p.GetValue(o));
        }

        public void Dispose()
        {
            RollbackTransaction();
            CloseConnection();
        }
    }
}
