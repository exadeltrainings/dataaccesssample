﻿namespace DataAccessSample.Data.AdoNet.Interfaces
{
    public interface ITransaction
    {
        void Commit();
        void Rollback();
    }
}
