﻿using System;
using System.Data;

namespace DataAccessSample.Data.AdoNet.Interfaces
{
    public interface ICommand : IDisposable
    {
        IDbCommand DbCommand { get; }
    }
}
