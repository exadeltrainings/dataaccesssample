﻿using System.Collections.Generic;
using System.Data;

namespace DataAccessSample.Data.AdoNet.Interfaces
{
    public interface IDataProvider
    {
        string ConnectionString { get; }
        
        int CommandTimeout { get; }

        IEnumerable<IDataRecord> ExecuteQuery(string queryText, object parameters = null);

        IEnumerable<IDataRecord> ExecuteProcedure(string procedureName, object parameters = null);

        T ExecuteScalar<T>(string queryText, object parameters = null);

        int ExecuteExec(string queryText, object parameters = null);
    }
}
