﻿using System;

namespace DataAccessSample.Data.AdoNet.Interfaces
{
    public interface IConnection : IDisposable
    {
        void Close();
    }
}
